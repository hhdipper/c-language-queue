#ifndef KEY_H
#define KEY_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"
#include "multi_button.h"
#include "includes.h"

#define KEY1 1
#define KEY2 2
#define KEY3 4
#define KEY4 8
#define KEY5 16
#define KEY6 32
#define KEY7 64
#define KEY8 128

extern void KeyInit(void);
extern int Get_keyVal();
extern void CLR_keyVal(u8 key);



#ifdef __cplusplus
}
#endif
#endif
