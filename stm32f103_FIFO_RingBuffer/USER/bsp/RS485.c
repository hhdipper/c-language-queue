#include "usart.h"
#include "RS485.h"
#include <string.h>//memset
#include "tim.H"

#include "app_FIFO.h"


static u16 const BUF_SIZE=512;
static uint8_t g_TxBuf1[BUF_SIZE];		/* 发送缓冲区 */
static uint8_t g_RxBuf1[BUF_SIZE];		/* 接收缓冲区 */

//---------------------------------------------------------------------------------------------------
HAL_StatusTypeDef RS485_DataErr();

// RS485 rs485_uart1;
RS485 rs485_uart2;

static void RS485_Receive(RS485_private*  _uart);
static void RS485_Transmit(RS485_private*  _uart);
static int GetData_State(RS485_private* _usart);
static void CLRData_State(RS485_private* _usart);
static void RS485_uartRX_CallBack(RS485* _usart);
//------------------------------------------------------------------------

void RS485_Receive(RS485_private*  _uart)
{

	HAL_UART_Receive_IT(_uart->m_uart,(u8*)&_uart->DMX_buf16,1);//接收一个双字节

}

static void RS485_uartRX_CallBack(RS485* _usart)
{

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

    if(huart->Instance==USART1)
    {
		// RS485_uartRX_CallBack(&rs485_uart1);
       
      
		
    
    }
	else if(huart->Instance==USART2)
	{}
	else if(huart->Instance==USART3)
    {
		// RS485_uartRX_CallBack(&rs485_uart2);
		u8*ch;
		HAL_UART_Receive_IT(&huart3,ch,1);//接收一个双字节
		// if (++_pUart->usRxWrite >= _pUart->usRxBufSize)
		// {
		// 	_pUart->usRxWrite = 0;
		// }
		// if (_pUart->usRxCount < _pUart->usRxBufSize)
		// {
		// 	_pUart->usRxCount++;
		// }

    }
}

static int GetData_State(RS485_private* _usart)
{
    return (GetBit(_usart->flag,Bit1));
}
static void CLRData_State(RS485_private* _usart)
{
	ClrBit(_usart->flag,Bit1);
}


void Init_RS485()
{

	RS485_1_TX();
	RS485_2_UART_RX;
	
//	FIFO_Init(&huart3,g_TxBuf1,g_RxBuf1,BUF_SIZE,BUF_SIZE);


}


void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
 {

    if(huart->Instance==USART2)
    {
            
        u32 checkFlag=0;
        // checkFlag= HAL_UART_GetError(huart);
        checkFlag= huart->ErrorCode;
        if(checkFlag)
        {
            //帧错误标志
            if(HAL_UART_ERROR_FE ==checkFlag)
            {
                __HAL_UART_CLEAR_FEFLAG(huart);
            
            }
            
            //溢出错误
            if(HAL_UART_ERROR_ORE ==checkFlag)
            {    

                __HAL_UART_CLEAR_OREFLAG(huart);

            }
        }
    }
    if(huart->Instance==USART3)
    {
            
        u32 checkFlag=0;
        // checkFlag= HAL_UART_GetError(huart);
        checkFlag= huart->ErrorCode;
        if(checkFlag)
        {
            //帧错误标志
            if(HAL_UART_ERROR_FE ==checkFlag)
            {
                __HAL_UART_CLEAR_FEFLAG(huart);
            
            }
            
            //溢出错误
            if(HAL_UART_ERROR_ORE ==checkFlag)
            {    

                __HAL_UART_CLEAR_OREFLAG(huart);

            }
        }
    }
 }

//空闲中断回调函数
void USER_UART_IDLECallback(UART_HandleTypeDef *huart)
{
     if(huart->Instance==USART2)
    {

    }

}

//-----------------------------------------------------------------------
#define TX_SIZE DMX_SIZE//

static void RS485_Transmit(RS485_private*  _uart)
{

    HAL_UART_Transmit_DMA(_uart->m_uart,(u8*)_uart->buf,TX_SIZE);

}
static void RS485_TX_CallBack(RS485* _usart)
{

	

}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    if(huart->Instance==USART1)
	{

		// Lamp_send_TxCpltCallback();
	}
    else if(huart->Instance==USART2)
	{
	}
    else if(huart->Instance==USART3)
    {
		// RS485_TX_CallBack(&rs485_uart2);

		 

    }
	

	
}

//--------------------------------------------------------------------------------------------------


#if(0)
#include<stdio.h>
int fputc(int ch,FILE *f)
{
	RS485_2_UART_TX;

	HAL_UART_Transmit(&huart3,(uint8_t *)&ch,1,0xff);
	// HAL_UART_Transmit_IT(&huart3,(uint8_t *)&ch,1);
	//  HAL_UART_Transmit_DMA(&huart3,(u8*)ch,1);
	return ch;
}

#endif
