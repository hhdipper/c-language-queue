#ifndef _INCLUDES_H
#define _INCLUDES_H

#include "main.h"


typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;
//无符
 
typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;

//==================================================================================
#define Bit0         0//
#define Bit1         1//
#define Bit2         2
#define Bit3         3
#define SetBit( Byte, Bit )  ( Byte ) |= ( 1<<( Bit ) )
#define ClrBit( Byte, Bit )  ( Byte ) &= ~( 1<<( Bit ) )
#define GetBit( Byte, Bit )  ( ( Byte ) & ( 1<<( Bit ) ) )
#define ComBit( Byte, Bit )  ( Bytes ) ^= ( 1<<( Bit ) )

#define BIT0         0x01
#define BIT1         0x02
#define BIT2         0x04
#define BIT3         0x08
#define SetBits( Byte, Bits ) ( Byte ) |= ( Bits )
#define ClrBits( Byte, Bits ) ( Byte ) &= ~( Bits )
#define GetBits( Byte, Bits ) ( ( Byte ) & ( Bits ) )
#define ComBits( Byte, Bits ) ( Byte ) ^= ( Bits )
//=================================================================================


#define SET_GPIO_H(x)               (x##_GPIO_Port->BSRR = x##_PIN) //GPIO_SetBits(x, x##_PIN)
#define SET_GPIO_L(x)               (x##_GPIO_Port->BRR  = x##_PIN) //GPIO_ResetBits(x, x##_PIN)
#define READ_GPIO_PIN(x)            (((x##_GPIO_Port->IDR & x##_PIN)!=Bit_RESET) ?1 :0) //GPIO_ReadInputDataBit(x, x##_PIN) 

#define SET_GPIO_0(X) 	X##_GPIO_Port->BSRR = (uint32_t)X##_Pin << 16u//0
#define SET_GPIO_1(X) 	X##_GPIO_Port->BSRR = X##_Pin//1
//=================================================================================

//----------------------- LED-指示 ---------------------------------------------------------------------------------------------
#define LED1_ON HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET)//绿色
#define LED1_OFF HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET)
#define LED1_Toggle HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin)

#define LED2_ON HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET)//蓝色
#define LED2_OFF HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET)
#define LED2_Toggle HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin)
#define LED_G_ON	LED1_ON
#define LED_G_OFF	LED1_OFF
#define LED_B_ON	LED2_ON
#define LED_B_OFF	LED2_OFF


//-------------------RS485-------------------------------------------------------------
//u4-USART1
#define RS485_1_RX() RS485_1_RD_GPIO_Port->BSRR = (uint32_t)RS485_1_RD_Pin << 16u//0
#define RS485_1_TX() RS485_1_RD_GPIO_Port->BSRR = RS485_1_RD_Pin//1

//u6-USART3
#define RS485_2_RX() RS485_2_RD_GPIO_Port->BSRR = (uint32_t)RS485_2_RD_Pin << 16u//0
#define RS485_2_TX() RS485_2_RD_GPIO_Port->BSRR = RS485_2_RD_Pin//1
//=================================================================================

//U5-Ch442E
#define CH442E_IN_0() 	CH442E_IN_GPIO_Port->BSRR = (uint32_t)CH442E_IN_Pin << 16u//0
#define CH442E_IN_1() 	CH442E_IN_GPIO_Port->BSRR = CH442E_IN_Pin//1

#define CH442E_IN_L()	SET_GPIO_L(CH442E_IN)
#define CH442E_IN_H()	SET_GPIO_H(CH442E_IN)

#define SET_CH442E_IN_0()	SET_GPIO_0(CH442E_IN)
#define SET_CH442E_IN_1()	SET_GPIO_1(CH442E_IN)
//=================================================================================


#endif
