
#ifndef RS485_H_
#define RS485_H_
#ifdef __cplusplus
 extern "C" {
#endif

#include "includes.h"

#define DMX_SIZE 514


typedef struct 
{
	uint16_t buf[DMX_SIZE];
	UART_HandleTypeDef* m_uart;
	__IO u8 flag;
	__IO u16 DMX_inode;
	__IO u16 DMX_buf16;
}RS485_private;
typedef struct 
{

	RS485_private	m_rs485;
	void (*Receive)(RS485_private*);
	void (*Transmit)(RS485_private*);
	int (*Get_State)(RS485_private*);
	void (*CLR_State)(RS485_private*);

}RS485;


extern void Init_RS485();

 


/*------------------- 外部功能函数-------------------*/
/* 
extern RS485 rs485_uart1;
#define RS485_1_ReceiveStart()		rs485_uart1.Receive		(&rs485_uart1.m_rs485)
#define RS485_1_GetData_State() 	rs485_uart1.Get_State	(&rs485_uart1.m_rs485)
#define RS485_1_CLRData_State() 	rs485_uart1.CLR_State	(&rs485_uart1.m_rs485)
#define RS485_1_Transmit() 			rs485_uart1.Transmit	(&rs485_uart1.m_rs485)
#define RS485_1_TxCpltCallback()	HAL_UART_TxCpltCallback	 (rs485_uart1.m_rs485.m_uart)
#define RS485_1_AbortTransmit()		HAL_UART_AbortTransmit_IT(rs485_uart1.m_rs485.m_uart)
#define RS 485_1_AbortReceive()		HAL_UART_AbortReceive_IT (rs485_uart1.m_rs485.m_uart)
*/
//------------------------------------------------------------------------------
extern RS485 rs485_uart2;
#define RS485_2_ReceiveStart()		rs485_uart2.Receive		(&rs485_uart2.m_rs485)
#define RS485_2_GetData_State() 	rs485_uart2.Get_State	(&rs485_uart2.m_rs485)
#define RS485_2_CLRData_State() 	rs485_uart2.CLR_State	(&rs485_uart2.m_rs485)
#define RS485_2_Transmit() 			rs485_uart2.Transmit	(&rs485_uart2.m_rs485)
#define RS485_2_TxCpltCallback()	HAL_UART_TxCpltCallback	(rs485_uart2.m_rs485.m_uart)
#define RS485_2_AbortTransmit()		HAL_UART_AbortTransmit_IT(rs485_uart2.m_rs485.m_uart)
#define RS485_2_AbortReceive()		HAL_UART_AbortReceive_IT (rs485_uart2.m_rs485.m_uart)


#define RS485_2_UART_TX		{RS485_2_TX();CH442E_IN_0();}
#define RS485_2_UART_RX		{RS485_2_RX();CH442E_IN_0();}


// #define CCT_ADJUST
#ifdef CCT_ADJUST
void CCT_Adjust();
#endif


#ifdef __cplusplus
}
#endif
#endif
