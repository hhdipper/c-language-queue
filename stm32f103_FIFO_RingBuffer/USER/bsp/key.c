#include "includes.h"
#include "key.h"
//#include "user.h"
//#include "Interface.h"



static u8 Key_val=0;

struct Button Key1, Key2, Key3, Key4,Key5, Key6;

/*声明按键功能事件的回调函数 */
void Key1_press_down_Handler(void *key1);
void Key2_press_down_Handler(void *key2);
void Key3_Callbakc(void *key3);
void Key4_Callbakc(void *key4);
void Key5_Callbakc(void *key5);
void Key6_Callbakc(void *key6);


/*读取按键电平*/
uint8_t read_key1()
{

    return HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin);
}

uint8_t read_key2()
{

    return HAL_GPIO_ReadPin(KEY2_GPIO_Port, KEY2_Pin);
}

uint8_t read_key3()
{

    return HAL_GPIO_ReadPin(KEY3_GPIO_Port, KEY3_Pin);
}

uint8_t read_key4()
{
    return HAL_GPIO_ReadPin(KEY4_GPIO_Port, KEY4_Pin);

}
uint8_t read_key5()//配对键取消
{
    return HAL_GPIO_ReadPin(KEY5_GPIO_Port, KEY5_Pin);

}
uint8_t read_key6()
{
    return HAL_GPIO_ReadPin(KEY6_GPIO_Port, KEY6_Pin);

}
void SETB_keyVal(u8 key)
{
	Key_val |= key;
}
int Get_keyVal()
{
	return Key_val;
}
void CLR_keyVal(u8 key)
{
	Key_val &=~key;
}
void NOT_keyVal(u8 key)
{
	Key_val ^=key;
}
//----------------------------------------------------------------

//----------------------------------------------------------------

void KeyInit(void)
{
    //初始化按键对象
    /*  第一个参数为刚刚创建的按键对象的指针；
        第二个参数为绑定按键的GPIO电平读取接口函数；
        第三个参数为设置有效触发电平；
     */
    button_init(&Key1, read_key1, 0);//M
    button_init(&Key2, read_key2, 0);//OJ
    button_init(&Key3, read_key3, 0);//-
    button_init(&Key4, read_key4, 0);//+
    button_init(&Key5, read_key5, 0);//+
    button_init(&Key6, read_key6, 0);//+

    //注册按钮事件
    /*
        第一个参数为按钮对象指针；
        第二个参数为MultiButton支持的按钮事件；
        第三个参数为要注册的该事件回调函数；
    */
    //注册按钮事件回调函数，每个事件都要单独注册
    button_attach(&Key1, PRESS_DOWN, Key1_press_down_Handler); //只有按下
    button_attach(&Key2, PRESS_DOWN, Key2_press_down_Handler); //只有按下

    button_attach(&Key3, PRESS_DOWN, Key3_Callbakc); //1个事件，按下          //1个事件,长按下只触发一次
    // button_attach(&Key3, PRESS_UP, Key3_Callbakc); //
    // button_attach(&Key3, LONG_PRESS_HOLD, Key3_Callbakc); //1个事件，长按

    button_attach(&Key4, PRESS_DOWN, Key4_Callbakc); //1个事件，按下
    // button_attach(&Key4, LONG_PRESS_HOLD, Key4_Callbakc); //1个事件，长按
    // button_attach(&Key4, PRESS_UP, Key4_Callbakc); //
    
	button_attach(&Key5, PRESS_DOWN, Key5_Callbakc); //只有按下
    button_attach(&Key6, PRESS_DOWN, Key6_Callbakc); //只有按下

    //按键开始
    button_start(&Key1);
    button_start(&Key2);
    button_start(&Key3);
    button_start(&Key4);
    button_start(&Key5);
    button_start(&Key6);


}

//---------------------------------------------------------------------------------------------

void Key1_press_down_Handler(void *key1)
{
//	if(Screensavers_state())
//	{    
//		Screensavers_clear();
//		return;
//	}
//	Screensavers_clear();

	SETB_keyVal(KEY1);

}


void Key2_press_down_Handler(void *key2)
{
//	if(Screensavers_state())
//	{
//		Screensavers_clear();
//		return;
//	}
//	Screensavers_clear();

	SETB_keyVal(KEY2);
    
}


void Key3_Callbakc(void *key3)
{

    uint32_t key_event_val;
    key_event_val = get_button_event((struct Button *)key3);

//------------------------------------------------
//   if(Screensavers_state() )
//   {
//       Screensavers_clear();
//       return;
//   }
//  Screensavers_clear();

//------------------------------------------------

    switch (key_event_val)
    {
        case PRESS_DOWN:
        {
			SETB_keyVal(KEY3);
        }
        break;
        case PRESS_UP:
        {
			// CLR_keyVal(KEY3);

        }
        break;
        case LONG_PRESS_HOLD:
        {
			SETB_keyVal(KEY3);
        }
        break;
        default:
            break;
    }

}

void Key4_Callbakc(void *key4)
{
    uint32_t key_event_val;
    key_event_val = get_button_event((struct Button *)key4);
    
//------------------------------------------------
//   if(Screensavers_state())
//   {
//       Screensavers_clear();
//       return;
//   }
//  Screensavers_clear();

//------------------------------------------------

    switch (key_event_val)
    {
        case PRESS_DOWN:
        {
			SETB_keyVal(KEY4);
        }break;
        case PRESS_UP:
        {
            
			CLR_keyVal(KEY4);
            
        }break;
        case LONG_PRESS_HOLD:
        {

			// SETB_keyVal(KEY4);
        }
        break;
        default:
            break;
    }

}

void Key5_Callbakc(void *key5)
{
//	if(Screensavers_state())
//	{    
//		Screensavers_clear();
//		return;
//	}
//	Screensavers_clear();

	// SETB_keyVal(KEY5);

}
void Key6_Callbakc(void *key6)
{
//	if(Screensavers_state())
//	{    
//		Screensavers_clear();
//		return;
//	}
//	Screensavers_clear();

	// NOT_keyVal(KEY6);
	SETB_keyVal(KEY6);

}
