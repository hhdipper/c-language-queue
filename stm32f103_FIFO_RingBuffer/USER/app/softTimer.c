#include "multi_timer.h"
#include<stdbool.h>

#include "multi_button.h"
//#include "Interface.h"
#include "includes.h"
#include "softTimer.h"
//#include "LED.h"
//#include "EC11.h"

static struct Timer timer1;
static struct Timer timer2;

static void timer1_callback();
static void timer2_callback();

//------------------------------------------------
__IO u8 itick=0;
__IO u8 itimer=0;
__IO u8 itimer_1s=0;
__IO u16 itimer_cnt=0;
__IO static char EC11_Dir=0;



/* 
10ms
 */
void timer1_callback()
{
	button_ticks();
	
//----------------------------
	itick++;
	if(10 ==itick)//10*10=100ms
	{
		itick=0;
//		Screensavers_start();//
		// LED_B_Toggle;

		if(itimer_1s++ >5)//5*100=500ms
		{
			itimer_1s=0;
			SetBit(itimer,Bit0);//DMX512

		}
	}
	
//----------------------------
	/* if(EC11_Direc())
	{
		EC11_Dir++;
		if(EC11_Dir>3)
		{
			EC11_Dir=0;

		}
	} */

#if BATTERY
	itimer_cnt++;
	if(100 ==itimer_cnt)//1000ms
	{
		itimer_cnt=0;
		
		SetBit(itimer,Bit1);//About()
	}
#endif

}
//------------------------------------------------
void timer2_callback()
{


//	LED_Effect();
	
} 
//------------------------------------------------

void Init_softTimer()
{

	timer_init(&timer1, timer1_callback, 10,10); //10ms loop周期
    timer_start(&timer1);
    
    timer_init(&timer2, timer2_callback, 1, 1); //1ms 周期
    timer_start(&timer2);

}
//---------------------------------

void Timer_CLR(u8 itimer_bit)
{
	ClrBit(itimer,itimer_bit);
}
bool Timer_GET(u8 itimer_bit)
{
	return GetBit(itimer,itimer_bit);
}

//---------------------------------

//---------------------------------

