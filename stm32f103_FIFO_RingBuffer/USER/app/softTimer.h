#ifndef _SOFTTIMER_H
#define _SOFTTIMER_H


void Init_softTimer();

//------------------------------------
#define	softTIM2(a)	{timer_init(&timer2, timer2_callback, a, a);timer_start(&timer2);}

//---------------------------------------------------
void Timer_CLR(u8 itimer_bit);
bool Timer_GET(u8 itimer_bit);

#endif
