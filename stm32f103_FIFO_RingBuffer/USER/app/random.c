
#include "includes.h"
#include "random.h"

/*
均匀分布的随机数
产生任意范围内的随机数，比如[m,n]之间的随机数
定义float m,n,r; m+(n-m)*rand_test(&r);
*/
float randa(float *r)
{
    float base = 256.0;
    float a = 17.0;
    float b = 139.0;
    float temp1 = a * (*r) + b;
    float temp2 = (s16)(temp1 / base);
    float temp3 = temp1 - temp2 * base;
    *r = temp3;
    float p = *r / base;
    return p;
}


/************************************
生成（min，max）区间上均匀分布随机数
	min		---给定区间下限
	max		---给定区间上限
	seed	---随机数种子
************************************/
float uniform(float min, float max, long int *seed)
{
	float t;
	*seed = 2045 * (*seed) + 1;
	*seed = *seed - (*seed / 1048576);
	t = (*seed) / 1048576.0;
	t = min+ (max - min) * t;
	return(t);
}


