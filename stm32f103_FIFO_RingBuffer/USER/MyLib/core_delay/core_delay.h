#ifndef __CORE_DELAY_H
#define __CORE_DELAY_H

#include "main.h"


/* 为方便使用，在延时函数内部调用CPU_TS_TmrInit函数初始化时间戳寄存器，
   这样每次调用函数都会初始化一遍。
   把本宏值设置为0，然后在main函数刚运行时调用 CPU_TS_TmrInit 可避免每次都初始化 */  

// #define CPU_TS_INIT_IN_DELAY_FUNCTION   1  

/*
最长 59.65232354166667
一次t=(1/72MHz) 13.888888888888888888888888888889ns 
 t*72 =1μs 
 t*36 =(1μs/2)=500ns 
 t*9 =125ns 
*/
/*******************************************************************************
* 函数声明
********************************************************************************/
#define  DWT_CYCCNT  			(*(__IO uint32_t *)0xE0001004)
#define	CPU_DWT_CNT_Clear()		(DWT_CYCCNT=0)
#define	CPU_DWT_CNT_Get()		((uint32_t)DWT_CYCCNT)
#define	CPU_DWT_CNT_125ns		(9)//125ns
#define	CPU_DWT_CNT_1s			(8000000*CPU_DWT_CNT_125ns	)//1s
#define	CPU_DWT_CNT_1ms			(8000*CPU_DWT_CNT_125ns		)//1ms
#define	CPU_DWT_CNT_1μs			(8*CPU_DWT_CNT_125ns		)//1μs
#define	CPU_DWT_CNT_500ns		(4*CPU_DWT_CNT_125ns		)//500ns




uint32_t CPU_TS_TmrRd(void);
HAL_StatusTypeDef HAL_DWTInit(uint32_t TickPriority);
uint32_t HAL_DWTGetTick(void);


//使用以下函数前必须先调用 CPU_TS_TmrInit 函数使能计数器，或使能宏CPU_TS_INIT_IN_DELAY_FUNCTION
//最大延时值为8秒
void CPU_TS_Tmr_Delay_US(uint32_t us);
#define CPU_TS_Tmr_Delay_MS(ms)     CPU_TS_Tmr_Delay_US(ms*1000)
#define CPU_TS_Tmr_Delay_S(s)       CPU_TS_Tmr_Delay_MS(s*1000)


#endif /* __CORE_DELAY_H */
