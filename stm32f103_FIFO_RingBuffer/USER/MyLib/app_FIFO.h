#ifndef APP_FIFO_H
#define APP_FIFO_H

// #define FIFI_2_POWER    
//======================================================================================================================

/* 串口设备结构体 */
typedef struct
{
	uint8_t *p_buf;			/* 缓冲区指针 */
	uint16_t buf_size;		/* 发送缓冲区大小 */


	__IO uint16_t read_pos;	/* 缓冲区读指针 */
	__IO uint16_t write_pos;		/* 缓冲区写指针 */
	__IO uint16_t Count;	/* 数据个数 */
#ifdef FIFI_2_POWER
    __IO uint16_t     buf_size_mask;
#endif
	
}FIFO_T;

//创建队列
extern int  FIFO_Init       (FIFO_T* p_fifo ,uint8_t* m_pBuf ,uint16_t buf_size);

//单字节操作
extern int app_fifo_Input   (FIFO_T* p_fifo, uint8_t byte);
extern int app_fifo_get     (FIFO_T* p_fifo, uint8_t *p_byte);
extern int app_fifo_peek    (FIFO_T* p_fifo, uint16_t index, uint8_t *p_byte);

//多字节读写
extern int app_fifo_read    (FIFO_T* p_fifo, uint8_t *p_byte_array, uint16_t *p_size);
extern int app_fifo_write   (FIFO_T* p_fifo, uint8_t const *p_byte_array, uint16_t *p_size);


#endif // 

