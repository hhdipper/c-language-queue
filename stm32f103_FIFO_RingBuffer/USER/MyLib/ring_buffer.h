#ifndef __RING_BUFFER_H_
#define __RING_BUFFER_H_

// #define RINGBUF_IRQ_SAFE
#ifdef RINGBUF_IRQ_SAFE

#define INIT_CRITICAL() uint32_t priMask = __get_PRIMASK()//屏蔽中断/异常
#define ENTER_CRITICAL() __set_PRIMASK(1)//关闭总中断
#define LEAVE_CRITICAL() __set_PRIMASK(priMask)
#else
#define INIT_CRITICAL()
#define ENTER_CRITICAL()
#define LEAVE_CRITICAL()

#endif


typedef struct
{
	uint8_t *pBuf;
	uint32_t size;
	uint32_t cnt;
	uint32_t rNdx;//读取标号（始终指向头部）
	uint32_t wNdx;//写入标号（始终指向尾端）
  
} ring_buffer_t, RINGBUFF_T;

#ifndef MIN
#define MIN(x,y)  ((x) < (y) ? (x) : (y))
#endif /* ifndef MIN */

extern int32_t RingBuf_Init(ring_buffer_t *pRB, uint8_t *pBuffer, uint32_t size);
extern int32_t RingBuf_Write(ring_buffer_t* pRB, const uint8_t *pcData, uint32_t dataBytes);
extern int32_t RingBuf_Write1Byte(ring_buffer_t* pRB, const uint8_t *pcData);
extern int32_t RingBuf_Read(ring_buffer_t* pRB, uint8_t *pData, uint32_t dataBytes);
extern int32_t RingBuf_Read1Byte(ring_buffer_t* pRB, uint8_t *pData);
extern int32_t RingBuf_Copy(ring_buffer_t* pRB, uint8_t *pData, uint32_t dataBytes);
extern int32_t RingBuf_Peek(ring_buffer_t* pRB, uint8_t **ppData);
extern int32_t RingBuf_Free(ring_buffer_t* pRB, uint32_t bytesToFree);
extern int32_t RingBuf_GetFreeBytes(ring_buffer_t* pRB);
extern int32_t RingBuf_GetUsedBytes(ring_buffer_t* pRB);
#define RingBuffer_Insert	RingBuf_Write
#define RingBuffer_Pop		RingBuf_Read

#endif
