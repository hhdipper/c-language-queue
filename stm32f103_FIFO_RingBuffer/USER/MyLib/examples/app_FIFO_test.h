#include "main.h"
#include "app_FIFO.h"
#include "stdio.h"

#define BUFSIZE			20
#define FIFO_BUFSIZE	10

uint8_t wBuf[BUFSIZE];
uint8_t rBuf[BUFSIZE];
uint8_t testdat[30];
uint8_t* pBuf;



FIFO_T txfifo;
uint16_t num=0;

void FIFO_tsetMain()
{
	pBuf=rBuf;
	for (size_t i = 0; i < BUFSIZE*2; i++)
		testdat[i]=i+1;

	FIFO_Init(&txfifo,wBuf,FIFO_BUFSIZE);

	uint8_t a=99;
	/*向 wBuf 写入99
	 write_pos 是1*/
	app_fifo_Input(&txfifo,a);
	/*向指向的队列写入3个数据，向 wBuf 写入1,2,3
	 write_pos 是4
	*/
	num=3;
	app_fifo_write(&txfifo,testdat,&num);

	/*向队列写入10个数据，已经超过队列的大小
	已经写入了3个这里其实只能写入6个，num将返回6，
	write_pos 是0
	*/
	num=10;
	if(app_fifo_write(&txfifo,testdat,&num) )
	 printf("成功写入 %d 个\r\n",num);

	/* 再向队列写入3个，
	队列已满，写入失败
	 */
	num=3;
	if(!app_fifo_write(&txfifo,testdat,&num) )
		printf("写入失败\r\n");

	/*读取一个数
	读数据指针是 1，
	写数据指针是 0 
	*/
	if(app_fifo_get(&txfifo,pBuf) )
	{
		printf("取出一个数 %d\r\n",*pBuf);
		pBuf+=1;
	}
	
	/* 
	查看位置是3的数据
	 */
	uint8_t pos =3,r=0;
	if(app_fifo_peek(&txfifo,pos,&r) )
		printf("位置%d的数是 %d\r\n",pos,r);

	/*
	取出3个数据，放在 rBuf[]中，
	读数据指针是 4，
	写数据指针是 0
	  */
	uint16_t n=3;
	if(app_fifo_read(&txfifo,pBuf,&n) )
	{
		printf("成功取出 %d 个\r\n",n);
		printf("读数据指针 %d 个\r\n",txfifo.read_pos);
		printf("写数据指针 %d 个\r\n",txfifo.write_pos);
		pBuf+=n;
	}

	/*
	取出10个数据，放在 rBuf[]中，
	实际可读取空间只有6个，
	读数据指针是 6+4，
	写数据指针是 0
	  */
	n=10;
	if(app_fifo_read(&txfifo,pBuf,&n) )
	{
		printf("成功取出 %d 个\r\n",n);
		printf("读数据指针 %d 个\r\n",txfifo.read_pos);
		printf("写数据指针 %d 个\r\n",txfifo.write_pos);
		pBuf+=n;
	}
//---------------------------------------------------------------

	num=7;
	if(app_fifo_write(&txfifo,testdat,&num))
	{
		printf("成功写入%d个",num);
		printf("读数据指针 %d 个\r\n",txfifo.read_pos);
		printf("写数据指针 %d 个\r\n",txfifo.write_pos);

	}

	num=3;
	pBuf=rBuf;
	if(app_fifo_read(&txfifo,pBuf,&num) )
	{
		printf("成功取出 %d 个\r\n",num);
		printf("读数据指针 %d 个\r\n",txfifo.read_pos);
		printf("写数据指针 %d 个\r\n",txfifo.write_pos);
		pBuf+=num;
	}

}
