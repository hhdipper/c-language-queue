#include "main.h"
#include "ring_buffer.h"

#define BUFSIZE			20
#define RING_BUFSIZE	10

uint8_t txBuf[BUFSIZE];
uint8_t rxBuf[BUFSIZE];
uint8_t testdat[30];
uint8_t* pBuf;



RINGBUFF_T txringBuff;


void Ring_buf_tsetMain()
{
	pBuf=rxBuf;
	for (size_t i = 0; i < BUFSIZE*2; i++)
		testdat[i]=i+1;

	RingBuf_Init(&txringBuff,txBuf,RING_BUFSIZE);

	//向 txBuf 写入1,2,3
	RingBuf_Write(&txringBuff,testdat,3);

	uint8_t a=99;// 在后面写入99,txBuf 里面是1,2,3,99,计数是4
	RingBuf_Write1Byte(&txringBuff,&a);//
	/* 
	// 在后面再写入1,2,3,缓冲区计数值是7
	写和读指针都是7
	 */
	RingBuf_Write(&txringBuff,testdat,3);

	/* 
	//从 txringBuff 取4个，放进 rxbuf，
	缓冲区计数值是3
	读指针是4
	写指针是7
	*/
	RingBuf_Read(&txringBuff,pBuf,4);
	pBuf+=4;
	/*从 txringBuff 再取1个，
	缓冲区计数值是2
	读指针是5
	写指针是7
	*/
	RingBuf_Read1Byte(&txringBuff,pBuf);
	pBuf+=1;
	/*
	从 txringBuff 再取4个，
	其实只能读取2个,
	缓冲区计数值是0
	写和读指针都是7
	*/
	RingBuf_Read(&txringBuff,pBuf,4);
	pBuf+=4;

	
	/* 
	向环形缓冲区写入10个
	缓冲区计数值是10
	先从7开始写到9，再从0开始写到7，
	写和读指针都是7，此时是满的
	 */
	RingBuf_Write(&txringBuff,testdat,10);
	
	/* 
	再向环形缓冲区写入3个
	因为是满的，数据保持不变
	 */
	RingBuf_Write(&txringBuff,testdat+10,3);
	/*
	释放10个数据位
	只清除了计数值，其它不变，下次写入时继续从此位置。
	*/
	RingBuf_Free(&txringBuff,10);

	/* 
	向环形缓冲区写入3个
	使写指针到0
	缓冲区计数值是3
	 */
	RingBuf_Write(&txringBuff,testdat+10,3);
	/* 
	从环形缓冲区读取3个
	使读指针也到0
	写指针不变是0
	缓冲区计数值是0
	 */
	RingBuf_Read(&txringBuff,pBuf,4);
	pBuf+=4;

	for (size_t i = 0; i < BUFSIZE; i++)
		rxBuf[i]=0;

	/* 
	向环形缓冲区写入15个，大于缓冲区容量
	实际最大能写入10个
	缓冲区计数值是15
	写和读指针都是7
	 */
	RingBuf_Write(&txringBuff,testdat,15);
	

}
