/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define KEY1_Pin GPIO_PIN_0
#define KEY1_GPIO_Port GPIOC
#define KEY4_Pin GPIO_PIN_1
#define KEY4_GPIO_Port GPIOC
#define KEY3_Pin GPIO_PIN_2
#define KEY3_GPIO_Port GPIOC
#define KEY2_Pin GPIO_PIN_3
#define KEY2_GPIO_Port GPIOC
#define KEY5_Pin GPIO_PIN_0
#define KEY5_GPIO_Port GPIOA
#define KEY6_Pin GPIO_PIN_1
#define KEY6_GPIO_Port GPIOA
#define SPI1_NSS_Pin GPIO_PIN_4
#define SPI1_NSS_GPIO_Port GPIOA
#define NTC_ADC_Pin GPIO_PIN_6
#define NTC_ADC_GPIO_Port GPIOA
#define RS485_2_RD_Pin GPIO_PIN_4
#define RS485_2_RD_GPIO_Port GPIOC
#define LCD_RES_Pin GPIO_PIN_5
#define LCD_RES_GPIO_Port GPIOC
#define LCD_RS_Pin GPIO_PIN_0
#define LCD_RS_GPIO_Port GPIOB
#define LCD_BLK_Pin GPIO_PIN_1
#define LCD_BLK_GPIO_Port GPIOB
#define NRF_SPI_CSN_Pin GPIO_PIN_12
#define NRF_SPI_CSN_GPIO_Port GPIOB
#define NRF_IRQ_Pin GPIO_PIN_6
#define NRF_IRQ_GPIO_Port GPIOC
#define NRF_CE_Pin GPIO_PIN_7
#define NRF_CE_GPIO_Port GPIOC
#define RS485_1_RD_Pin GPIO_PIN_8
#define RS485_1_RD_GPIO_Port GPIOC
#define CH442E_IN_Pin GPIO_PIN_9
#define CH442E_IN_GPIO_Port GPIOC
#define PWM_FAN_Pin GPIO_PIN_8
#define PWM_FAN_GPIO_Port GPIOA
#define DMX_TX_Pin GPIO_PIN_11
#define DMX_TX_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_2
#define LED2_GPIO_Port GPIOD
#define LED1_Pin GPIO_PIN_3
#define LED1_GPIO_Port GPIOB
#define AT24C04_SDA_Pin GPIO_PIN_4
#define AT24C04_SDA_GPIO_Port GPIOB
#define AT24C04_SCL_Pin GPIO_PIN_5
#define AT24C04_SCL_GPIO_Port GPIOB
#define EC11_A_Pin GPIO_PIN_6
#define EC11_A_GPIO_Port GPIOB
#define EC11_B_Pin GPIO_PIN_7
#define EC11_B_GPIO_Port GPIOB
#define DMX_RX1_Pin GPIO_PIN_8
#define DMX_RX1_GPIO_Port GPIOB
#define DMX_RX2_Pin GPIO_PIN_9
#define DMX_RX2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
